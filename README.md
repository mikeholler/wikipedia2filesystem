wikipedia2filesystem
====================

This is a small project that can take any(?) XML dump from http://dumps.wikimedia.org/
and unpack it into files hashed by name into a directory structure. With the largest
dump on Wikipedia (all full articles from en.wikipedia.org) the process ended with
only about 50 files in each sub folder. Impressive, given the very large numbers
of articles in Wikipedia.

A **file** represents a single Wikipedia article. Unfortunately, redirects are
considered articles, so there are many files with just a redirect line in them.

Please note that this script takes a *long* time to run. On an i5 laptop with 8GB of
memory and a 5200RPM hard drive, the script took almost exactly 24 hours to complete.
Make sure you have some time on your hands.

Running the Program
-------------------

Simply execute `python wikixmldump2files.py /path/to/dump.xml /where/you/want/these/files/`
and the (long) process will begin.

Finding an Article
------------------

So you've finished running the script, and you want to find where the article is? No problem.
All you'll have to do is run `python article_retrieve.py ARTICLE_NAME` to get the article.
`ARTICLE_NAME` corresponds to the last segment of the path of a Wikipedia page. For example,
the article for Monty Python, located at http://en.wikipedia.org/wiki/Monty_Python can be
found and printed to the screen by typing:

```bash
python article_retrieve.py Monty_Python
```

Happy parsing!
