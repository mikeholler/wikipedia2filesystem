#!/usr/bin/python
__author__ = 'mjholler'

import os
import xml.sax
import hashlib
import os_extended

# Custom libraries
from progress_bar import ProgressBar

count = 0
ARTICLES_IN_DUMP = 12302175
progress = ProgressBar(ARTICLES_IN_DUMP)

def writeArticle(root, title, text):
    global count

    progress.update_progress_bar(count)
    count += 1

    # Wikipedia-ize the title of the file name
    title = title.encode("UTF-8")
    title = title.replace(" ", "_")

    # Special case for /: "%x" % ord ("/") == 2f
    title = title.replace("/", "%2F")

    # Store files in directories based on md5 hash of title
    md5_hash = hashlib.md5(title).hexdigest()
    parent = md5_hash[:2]
    child = md5_hash[2:4]

    # Form path to article's folder or create it if it doesn't exist
    article_path = os.path.join(root, parent, child)
    if not os.path.exists(article_path):
        os_extended.mkdir_p(article_path)

    # Write article to file if it hasn't been already. Comment out if statement if you are using a new dump
    if not os.path.exists(os.path.join(article_path, title + ".txt")) and len(title + ".txt") <= 256:
        article_path = os.path.join(article_path, title + ".txt")
        article_file = open(article_path, 'w')
        article_file.write(text.encode('utf8'))
        article_file.close()

class WikiPageSplitter(xml.sax.ContentHandler):
    def __init__(self, root):
        self.root = root
        self.stack = []
        self.text = None
        self.title = None

    def startElement(self, name, attributes):
        if name == "page":
            assert self.stack == []
            self.text = None
            self.title = None
        elif name == "title":
            assert self.stack == ["page"]
            assert self.title is None
            self.title = ""
        elif name == "text":
            assert self.stack == ["page"]
            assert self.text is None
            self.text = ""
        else:
            assert len(self.stack) == 0 or self.stack[-1] == "page"
            return

        self.stack.append(name)

    def endElement(self, name):
        if len(self.stack) > 0 and name == self.stack[-1]:
            del self.stack[-1]

        if name == "text":
            # We have the complete article: write it out
            writeArticle(self.root, self.title, self.text)

    def characters(self, content):
        assert content is not None and len(content) > 0
        if len(self.stack) == 0:
            return

        if self.stack[-1] == "title":
            self.title += content
        elif self.stack[-1] == "text":
            assert self.title is not None
            self.text += content
