#!/usr/bin/python

# Python libraries
import sys
import time
import xml.sax

# Custom libraries
from wiki_parser import WikiPageSplitter

start_time = time.time()
xml.sax.parse(sys.argv[1], WikiPageSplitter(sys.argv[2]))
print "This took %s seconds." %(time.time()-start_time)
