#!/usr/bin/python
__author__ = 'mjholler'

import time

class Timer(object):
    def __init__(self):
        self.__start_time = time.time()
        self.__secs_since_interval_called = self.__start_time

    def time_elapsed(self, format=False):
        # Returns string of total seconds elapsed
        if format is False:
            return time.time() - self.__start_time
        # Returns string in format dd:hh:mm:ss
        if format is True:
            total_seconds = round(time.time() - self.__start_time)
            secs = round(total_seconds%60)
            mins = round((total_seconds/60)%60)
            hrs = round((total_seconds/3600)%24)
            days = round(total_seconds/(3600*24))
            return "%02d:%02d:%02d:%02d" %(int(days), int(hrs), int(mins), int(secs))

    def reset(self):
        self.__start_time = time.time()

    def interval_passed(self, secs):
        if time.time() - self.__secs_since_interval_called > secs:
            self.__secs_since_interval_called = time.time()
            return True
        return False
