#!/usr/bin/python
#
__author__ = 'mjholler'

import urllib
import os
from sys import argv

path = argv[1]
outfile_path = argv[2]
titles = []

article_count = 0
total_articles = 12302175
one_percent_of_articles = 12302175/100
perc = 0

dirList = os.listdir(path)

print "Gathering titles..."
for fname in dirList:
    title_file = open(os.path.join(path, fname))
    titles += title_file.readlines()
    title_file.close()

print "Decoding titles..."
urldecoded_titles = []
for title in titles:
    urldecoded_titles.append(urllib.unquote(title)[:-5])

    if len(urldecoded_titles) % one_percent_of_articles == 0:
        perc += 1
        print "%s percent done... %s" %(perc, len(urldecoded_titles))
print len(urldecoded_titles)
print "Sorting titles..."
urldecoded_titles.sort()

article_count = 0
perc = 0
print "Writing titles..."
outfile = open(outfile_path, 'w')
for title in urldecoded_titles:
    article_count += 1
    if article_count % one_percent_of_articles == 0:
        perc += 1
        print "%s percent done... %s" %(perc, article_count)
    outfile.write("%s\n" %title)
outfile.close()

print "Done."
