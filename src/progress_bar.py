#!/usr/bin/python
__author__ = 'mjholler'

import os
import sys
import time
from timer import Timer

class ProgressBar(object):

    def __init__(self, total):
        self.console_width = self.__getTerminalSize()[0]
        assert(self.console_width >= 10)
        # The 7 represents necessary formatting, the 12 represents the time
        self.max_bar_width = self.console_width - 7 - 12
        self.current_bar_width = 0
        self.total = total
        self.start_time = time.time()
        self.stopwatch = Timer()
        # Progress bar format:
        # [========-               ] dd:hh:mm:ss 40%
        self.progress_bar = self.__str_mult(" ", self.max_bar_width)
        self.update_progress_bar(0)

    def __del__(self):
        self.update_progress_bar(self.total)

    def update_progress_bar(self, part):
        percentage = part*100/self.total
        if self.stopwatch.interval_passed(5) or self.__update_bar(part):
            print "[%s] %s %d%% \r" %(self.progress_bar, self.stopwatch.time_elapsed(True), percentage),
            sys.stdout.flush()

    def __update_bar(self, part):
        new_width = int((part*self.max_bar_width)/self.total)
        if new_width > self.current_bar_width or new_width == 0:
            self.current_bar_width = new_width
            self.progress_bar = self.__str_mult("=", self.current_bar_width)\
            + self.progress_bar[self.current_bar_width:]
            return True
        return False

    def __str_mult(self, s, times):
        multiplied_string = ""
        assert(times >= 0)
        for i in range(times):
            multiplied_string += s
        return multiplied_string

    def __getTerminalSize(self):
        def ioctl_GWINSZ(fd):
            try:
                import fcntl, termios, struct, os
                cr = struct.unpack('hh', fcntl.ioctl(fd, termios.TIOCGWINSZ,
                                                     '1234'))
            except:
                return None
            return cr
        cr = ioctl_GWINSZ(0) or ioctl_GWINSZ(1) or ioctl_GWINSZ(2)
        if not cr:
            try:
                fd = os.open(os.ctermid(), os.O_RDONLY)
                cr = ioctl_GWINSZ(fd)
                os.close(fd)
            except:
                pass
        if not cr:
            try:
                cr = (env['LINES'], env['COLUMNS'])
            except:
                cr = (25, 80)
        return int(cr[1]), int(cr[0])

