#!/usr/bin/python

__author__ = 'mjholler'

import os
import hashlib
from sys import argv

article_title = argv[1]
if len(argv) == 3:
    base_path = argv[2]
else:
    base_path = "/home/mjholler/Desktop/Projects/wikiProject/wikipedia/"

md5_hash = hashlib.md5(article_title).hexdigest()
parent = md5_hash[:2]
child = md5_hash[2:4]

article_path = os.path.join(base_path, parent, child, article_title + ".txt")
if os.path.exists(article_path):
    article_file = open(article_path)
    print(article_file.read())
    article_file.close()
else:
    print "Article does not exist."